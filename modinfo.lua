--[[
Copyright (C) 2018 Zarklord

This file is part of The Laziest Deserter.

The Laziest Deserteris free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Laziest Deserter.  If not, see <http://www.gnu.org/licenses/>.
]]

name = "The Laziest Deserter"
description = "The Lazy Deserter with a side of Lazy\n\nThis Allows The Lazy Deserter to finally send you from the caves to the overworld and vice versa.\n\nRequires Gem Core."
author = "Zarklord"
version = "1.1.1"
forumthread = ""

restart_required = false

dst_compatible = true

api_version_dst = 10

icon_atlas = "laziestdeserter.xml"
icon = "laziestdeserter.tex"

all_clients_require_mod = true
client_only_mod = false


server_filter_tags = 
{ 
    "laziestdeserter"
}

configuration_options = {}