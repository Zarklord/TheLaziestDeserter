--[[
Copyright (C) 2018 Zarklord

This file is part of The Laziest Deserter.

The Laziest Deserteris free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Laziest Deserter.  If not, see <http://www.gnu.org/licenses/>.
]]

local STATUS = {
    ACTIVE = 0,
    INACTIVE = 1,
}

local function onstatus(self, val)
    if val == STATUS.ACTIVE then
        self.inst:AddTag("townportalmigrator")
    else
        self.inst:RemoveTag("townportalmigrator")
    end
end

local LazyWorldMigrator = Class(function(self, inst)
    self.inst = inst

    self._status = -1

    self.id = nil

    self.linkedWorld = nil

    self.offset = nil

    self.onActivate = nil
end,
nil,
{
    _status = onstatus,
})

function LazyWorldMigrator:SetSender(world, id)
    self.id = id
    self.linkedWorld = world
    self:ValidateAndPushEvents()
end

function LazyWorldMigrator:SetReceiver(id)
    self.id = id
    self:ValidateAndPushEvents()
end

function LazyWorldMigrator:Reset()
    self.id = nil
    self.linkedWorld = nil
    self:ValidateAndPushEvents()
end

function LazyWorldMigrator:GetStatusString()
    return string.lower(tostring(table.reverselookup(STATUS, self._status)))
end

function LazyWorldMigrator:ValidateAndPushEvents()
    if self.linkedWorld ~= nil then
        self._status = STATUS.ACTIVE
    else
        self._status = STATUS.INACTIVE
    end
end

function LazyWorldMigrator:Activate(doer)
    print("Activating townportal["..self.id.."] to worldid: "..(self.linkedWorld or "<nil>"))
    if self.linkedWorld == nil then
        return false, "NODESTINATION"
    end
    if self.onActivate ~= nil then
        self.onActivate(self.inst, doer)
    end
    SendShardRPC(SHARD_RPC.LaziestDeserter.TownPortalUse, {tonumber(self.linkedWorld)}, self.id)
    TheWorld:PushEvent("ms_playerdespawnandtelemigrate", { player = doer, townportalid = self.id, worldid = self.linkedWorld })
    return true
end

function LazyWorldMigrator:GetDebugString()
    return string.format("ID: %d world: %s available: %s receives: %d status: %s", self.id or -1, self.linkedWorld or "<nil>", tostring(self.linkedWorld and Shard_IsWorldAvailable(self.linkedWorld) or false), self.id or -1, self:GetStatusString())
end

return LazyWorldMigrator
