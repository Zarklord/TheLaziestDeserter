--[[
Copyright (C) 2018 Zarklord

This file is part of The Laziest Deserter.

The Laziest Deserteris free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Laziest Deserter.  If not, see <http://www.gnu.org/licenses/>.
]]

--lets get that nice global enviroment for post init functions
GLOBAL.setfenv(1, GLOBAL)

MODENV.AddComponentPostInit("townportalregistry", function(townportalregistry)

	local inst = townportalregistry.inst

	local old_OnRegisterTownPortal = inst.event_listening["ms_registertownportal"][inst][1]
	local old_OnTownPortalActivated = inst.event_listening["townportalactivated"][inst][1]
	local old_OnTownPortalDeactivated = inst.event_listening["townportaldeactivated"][inst][1]
	local OnRemoveTownPortal = UpvalueHacker.GetUpvalue(old_OnRegisterTownPortal, "OnRemoveTownPortal")

	local _townportals = UpvalueHacker.GetUpvalue(old_OnRegisterTownPortal, "_townportals")
	local _activetownportal = UpvalueHacker.GetUpvalue(old_OnRegisterTownPortal, "_activetownportal")
	local _activeshardtownportal = nil
	local _townportalIds = {}
	local _activeshardtransfers = {}

	local function GetNewShardTownPortalID()
		local i = 1
		while true do
			if (_activeshardtransfers[i] or 0) == 0 and not (_activetownportal and _activetownportal.components.lazyworldmigrator.id == i) then
				return i
			end
			i = i + 1
		end
	end

	local function new_OnRegisterTownPortal(inst, townportal)
		--print("townportal registered")
		if _activeshardtownportal ~= nil then
		    for i, v in ipairs(_townportals) do
		        if v == townportal then
		            return
		        end
		    end

		    table.insert(_townportals, townportal)
		    inst:ListenForEvent("onremove", OnRemoveTownPortal, townportal)
		    --we need to wait a tiny bit of time for the event listener to get added
		    inst:DoTaskInTime(0, function() townportal:PushEvent("linkshardtownportals", _activeshardtownportal) end)
		else
			old_OnRegisterTownPortal(inst, townportal)
		end
	end

	local function new_OnTownPortalActivated(inst, townportal)
		---print("townportal activated")
		local shardtownportalid = GetNewShardTownPortalID()
		SendShardRPC(SHARD_RPC.LaziestDeserter.TownPortalActive, nil, shardtownportalid)
    	townportal.components.lazyworldmigrator:SetReceiver(shardtownportalid)
    	_townportalIds[shardtownportalid] = townportal
		old_OnTownPortalActivated(inst, townportal)
	end

	local function new_OnTownPortalDeactivated(inst, portal)
		--print("townportal deactivated")
		SendShardRPC(SHARD_RPC.LaziestDeserter.TownPortalDeactive, nil)
    	if _activetownportal then _activetownportal.components.lazyworldmigrator:Reset() end
		old_OnTownPortalDeactivated(inst, portal)
	end

	local function OnShardTownPortalActivated(inst, shardtownportal)
		if _activeshardtownportal == nil then
			_activeshardtownportal = shardtownportal
			for i, v in ipairs(_townportals) do
	    		v:PushEvent("linkshardtownportals", shardtownportal)
	    	end
	    end
	end

	local function OnShardTownPortalDeactivated(inst, sharddata)
		if _activeshardtownportal ~= nil and _activeshardtownportal.worldId == sharddata.worldId then
			_activeshardtownportal = nil
			for i, v in ipairs(_townportals) do
				v:PushEvent("linkshardtownportals")
			end
		end	
	end

	inst:ListenForEvent("shardtownportaluse", function(townportalid)
		_activeshardtransfers[townportalid] = (_activeshardtransfers[townportalid] or 0) + 1
	end)

	inst:ListenForEvent("shardtownportalused", function(townportalid)
		_activeshardtransfers[townportalid] = (_activeshardtransfers[townportalid] or 0) - 1
		if _activeshardtransfers[townportalid] < 0 then _activeshardtransfers[townportalid] = 0 end
	end)

	inst:ListenForEvent("shardtownportalactivated", OnShardTownPortalActivated)
	inst:ListenForEvent("shardtownportaldeactivated", OnShardTownPortalDeactivated)

	function townportalregistry:GetTownPortalFromId(id)
		return _townportalIds[id] or nil
	end

	UpvalueHacker.SetUpvalue(old_OnRegisterTownPortal, new_OnTownPortalDeactivated, "OnRemoveTownPortal", "OnTownPortalDeactivated")
	inst.event_listening["ms_registertownportal"][inst][1] = new_OnRegisterTownPortal
	inst.event_listeners["ms_registertownportal"][inst][1] = new_OnRegisterTownPortal
	inst.event_listening["townportalactivated"][inst][1] = new_OnTownPortalActivated
	inst.event_listeners["townportalactivated"][inst][1] = new_OnTownPortalActivated
	inst.event_listening["townportaldeactivated"][inst][1] = new_OnTownPortalDeactivated
	inst.event_listeners["townportaldeactivated"][inst][1] = new_OnTownPortalDeactivated
end)

MODENV.AddComponentPostInit("playerspawner", function(playerspawner)
	local inst = playerspawner.inst

	--despawning:
	local function PlayerRemove(player, deletesession, migrationdata, readytoremove)
	    if readytoremove then
	        player:OnDespawn()
	        if deletesession then
	            DeleteUserSession(player)
	        else
	            player.migration = migrationdata ~= nil and {
	                worldid = TheShard:GetShardId(),
	                townportalid = migrationdata.townportalid,
	                sessionid = TheWorld.meta.session_identifier,
	            } or nil
	            SerializeUserSession(player)
	        end
	        player:Remove()
	        if migrationdata ~= nil then
	            TheShard:StartMigration(migrationdata.player.userid, migrationdata.worldid)
	        end
	    else
	        player:DoTaskInTime(0, PlayerRemove, deletesession, migrationdata, true)
	    end
	end

	local function OnPlayerDespawn(inst, player, cb)
	    player.components.playercontroller:Enable(false)
	    player.components.locomotor:StopMoving()
	    player.components.locomotor:Clear()

	    --Portal FX
	    local fx = SpawnPrefab("spawn_fx_medium")
	    if fx ~= nil then
	        fx.Transform:SetPosition(player.Transform:GetWorldPosition())
	    end

	    --After colour tween, remove player via task, because
	    --we don't want to remove during component update loop
	    player.components.colourtweener:StartTween({ 0, 0, 0, 1 }, 13 * FRAMES, cb)
	end

	local function OnPlayerDespawnAndTeleMigrate(inst, data)
	    OnPlayerDespawn(inst, data.player, function(player) PlayerRemove(player, false, data) end)
	end

	inst:ListenForEvent("ms_playerdespawnandtelemigrate", OnPlayerDespawnAndTeleMigrate)

	--spawning:
	local GetNextSpawnPosition = UpvalueHacker.GetUpvalue(playerspawner.SpawnAtNextLocation, "GetNextSpawnPosition")

	local function NoHoles(pt)
	    return not TheWorld.Map:IsPointNearHole(pt)
	end

	local function NoPlayersOrHoles(pt)
	    return not (IsAnyPlayerInRange(pt.x, 0, pt.z, 2) or TheWorld.Map:IsPointNearHole(pt))
	end

	local function GetDestinationPortalLocation(player)
	    local portal = nil
	    if player.migration.worldid ~= nil and player.migration.townportalid ~= nil then
	        portal = TheWorld.components.townportalregistry:GetTownPortalFromId(player.migration.townportalid)
	    end

	    if portal ~= nil then
	        print("Player will spawn close to shardtownportal #"..tostring(portal.components.lazyworldmigrator.id))
	        local target_x, target_y, target_z = portal.Transform:GetWorldPosition()
	        local offset = portal.components.lazyworldmigrator ~= nil and portal.components.lazyworldmigrator.offset or 0
	        if offset ~= 0 then
	            local pt = Vector3(target_x, target_y, target_z)
	            local angle = math.random() * 2 * PI
	            offset =
	                FindWalkableOffset(pt, angle, offset, 8, true, false, NoPlayersOrHoles) or
	                FindWalkableOffset(pt, angle, offset * .5, 6, true, false, NoPlayersOrHoles) or
	                FindWalkableOffset(pt, angle, offset, 8, true, false, NoHoles) or
	                FindWalkableOffset(pt, angle, offset * .5, 6, true, false, NoHoles)
	            if offset ~= nil then
	                target_x = target_x + offset.x
	                target_z = target_z + offset.z
	            end
	        end
	        return target_x, target_y, target_z
	    else
	        print("Player will spawn at default location")
	        return GetNextSpawnPosition()
	    end
	end

	local old_SpawnAtLocation = playerspawner.SpawnAtLocation
	playerspawner.SpawnAtLocation = function(self, inst, player, x, y, z, isloading)
		if player.migration ~= nil and player.migration.townportalid ~= nil then
	        -- make sure we're not just back in our
	        -- origin world from a failed migration
	        if player.migration.worldid ~= TheShard:GetShardId() then
	            x, y, z = GetDestinationPortalLocation(player)
	            for i, v in ipairs(player.migrationpets) do
	                if v:IsValid() then
	                    if v.Physics ~= nil then
	                        v.Physics:Teleport(x, y, z)
	                    elseif v.Transform ~= nil then
	                        v.Transform:SetPosition(x, y, z)
	                    end
	                end
	            end
	        end
	        local townportalid = player.migration.townportalid
	        player.migration = nil
	        player.migrationpets = nil

		    print(string.format("Spawning player at: [%s] (%2.2f, %2.2f, %2.2f)", "Load", x, y, z))
		    player.Physics:Teleport(x, y, z)
		    if player.components.areaaware ~= nil then
		        player.components.areaaware:UpdatePosition(x, y, z)
		    end

		    -- Spawn a light if it's dark
		    if not inst.state.isday and #TheSim:FindEntities(x, y, z, 4, { "spawnlight" }) <= 0 then
		        SpawnPrefab("spawnlight_multiplayer").Transform:SetPosition(x, y, z)
		    end

		    -- Portal FX, disable/give control to player if they're loading in
	        player.AnimState:SetMultColour(0,0,0,1)
	        player:Hide()
	        player.components.playercontroller:Enable(false)
	        local fx = SpawnPrefab("spawn_fx_medium")
	        if fx ~= nil then
	            fx.entity:SetParent(player.entity)
	        end
	        player:DoTaskInTime(6*FRAMES, function(inst)
		        player.sg:GoToState("exitshardtownportal_pre")
		        TheWorld.components.townportalregistry:GetTownPortalFromId(townportalid):PushEvent("doneteleporting", player)
	            player.components.colourtweener:StartTween({1,1,1,1}, 19*FRAMES)
	        end)
			TheWorld:PushEvent("shardtownportalused", townportalid)
		else
			old_SpawnAtLocation(self, inst, player, x, y, z, isloading)
		end
	end
end)

MODENV.AddPrefabPostInit("townportal", function(townportal)

	--copied local functions start
	local function StartSoundLoop(inst)
	    if not inst.playingsound then
	        inst.playingsound = true
	        inst:OnEntityWake()
	    end
	end

	local function StopSoundLoop(inst)
	    if inst.playingsound then
	        inst.playingsound = nil
	        inst.SoundEmitter:KillSound("active")
	    end
	end
	--copied local functions end

	townportal:AddComponent("lazyworldmigrator")
	townportal.components.lazyworldmigrator.onActivate = townportal.components.teleporter.onActivate
	townportal.components.lazyworldmigrator.offset = 2

	townportal:ListenForEvent("linkshardtownportals", function(inst, other)
    	inst.components.lazyworldmigrator:SetSender(other and other.worldId or nil, other and other.townportalId or nil)

	    if inst.components.channelable:IsChanneling() then
	        inst.components.channelable:StopChanneling(true)
	    end
	    inst.components.channelable:SetEnabled(other == nil)

	    if other ~= nil then
	        inst.AnimState:PlayAnimation("turn_on")
	        inst.AnimState:PushAnimation("idle_on_loop")
	        StartSoundLoop(inst)
	    else
	        inst.AnimState:PlayAnimation("turn_off")
	        inst.AnimState:PushAnimation("idle_off")
	        StopSoundLoop(inst)
	    end
	end)
end)

MODENV.AddPrefabPostInit("townportaltalisman", function(townportaltalisman)

	--copied local functions start
	local function StartSoundLoop(inst)
	    if not inst.playingsound then
	        inst.playingsound = true
	        inst:OnEntityWake()
	    end
	end

	local function StopSoundLoop(inst)
	    if inst.playingsound then
	        inst.playingsound = nil
	        inst.SoundEmitter:KillSound("active")
	    end
	end

	local function DoActiveAnim(inst)
	    inst.onanimqueueover = nil
	    inst:RemoveEventCallback("animqueueover", DoActiveAnim)
	    inst.AnimState:PlayAnimation("active_loop", true)
	end

	local function DoRiseAnims(inst)
	    inst.animtask = nil
	    inst.onanimqueueover = DoActiveAnim
	    inst:ListenForEvent("animqueueover", DoActiveAnim)
	    inst.AnimState:PlayAnimation("active_rise")
	end

	local function DoInactiveAnim(inst)
	    inst.onanimqueueover = nil
	    inst:RemoveEventCallback("animqueueover", DoInactiveAnim)
	    inst.AnimState:PlayAnimation("inactive")
	end

	local function DoFallAnims(inst)
	    inst.animtask = nil
	    inst.onanimqueueover = DoInactiveAnim
	    inst:ListenForEvent("animqueueover", DoInactiveAnim)
	    inst.AnimState:PlayAnimation("active_fall")
	end
	--copied local functions end

	townportaltalisman:AddComponent("lazyworldmigrator")
	townportaltalisman.components.lazyworldmigrator.onActivate = townportaltalisman.components.teleporter.onActivate

	townportaltalisman:ListenForEvent("linkshardtownportals", function(inst, other)
    	inst.components.lazyworldmigrator:SetSender(other and other.worldId or nil, other and other.townportalId or nil)

	    if inst.animtask ~= nil then
	        inst.animtask:Cancel()
	        inst.animtask = nil
	    elseif inst.onanimqueueover ~= nil then
	        inst:RemoveEventCallback("animqueueover", inst.onanimqueueover)
	        inst.onanimqueueover = nil
	    end

	    if other ~= nil then
	        inst.components.inventoryitem:ChangeImageName("townportaltalisman_active")
	        if inst.components.inventoryitem:IsHeld() then
	            inst.AnimState:PlayAnimation("active_loop", true)
	            inst.AnimState:SetTime(math.random() * inst.AnimState:GetCurrentAnimationLength())
	        else
	            if inst.AnimState:IsCurrentAnimation("active_shake2") then
	                inst.AnimState:PlayAnimation("active_loop", true)
	                inst.AnimState:SetTime(math.random() * inst.AnimState:GetCurrentAnimationLength())
	            elseif inst.AnimState:IsCurrentAnimation("active_fall") then
	                inst.onanimqueueover = DoActiveAnim
	                inst:ListenForEvent("animqueueover", DoActiveAnim)
	                inst.AnimState:PushAnimation("active_rise", false)
	            else
	                inst.AnimState:PlayAnimation("active_shake", true)
	                inst.animtask = inst:DoTaskInTime(.2 + math.random() * .4, DoRiseAnims)
	            end
	        end
	        StartSoundLoop(inst)
	    else
	        inst.components.inventoryitem:ChangeImageName("townportaltalisman")
	        if inst.components.inventoryitem:IsHeld() or inst.AnimState:IsCurrentAnimation("active_shake") then
	            inst.AnimState:PlayAnimation("inactive")
	        elseif inst.AnimState:IsCurrentAnimation("active_rise") then
	            inst.onanimqueueover = DoInactiveAnim
	            inst:ListenForEvent("animqueueover", DoInactiveAnim)
	            inst.AnimState:PushAnimation("active_fall", false)
	        else
	            inst.AnimState:PlayAnimation("active_shake2", true)
	            inst.animtask = inst:DoTaskInTime(.3 + math.random() * .3, DoFallAnims)
	        end
	        StopSoundLoop(inst)
	    end
	end)
end)