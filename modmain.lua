--[[
Copyright (C) 2018 Zarklord

This file is part of The Laziest Deserter.

The Laziest Deserteris free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Laziest Deserter.  If not, see <http://www.gnu.org/licenses/>.
]]

GLOBAL.SetupGemCoreEnv(env)

if GLOBAL.TheNet:GetIsServer() then
	local function TownPortalActive(shard_id, townportalId)
		GLOBAL.TheWorld:PushEvent("shardtownportalactivated", {worldId = shard_id, townportalId = townportalId})
	end
	GLOBAL.AddShardRPCHandler("LaziestDeserter", "TownPortalActive", TownPortalActive)

	local function TownPortalDeactive(shard_id)
		GLOBAL.TheWorld:PushEvent("shardtownportaldeactivated", {worldId = shard_id})
	end
	GLOBAL.AddShardRPCHandler("LaziestDeserter", "TownPortalDeactive", TownPortalDeactive)

	local function TownPortalUse(shard_id, townportalId)
		GLOBAL.TheWorld:PushEvent("shardtownportaluse", townportalId)
	end
	GLOBAL.AddShardRPCHandler("LaziestDeserter", "TownPortalUse", TownPortalUse)

	modimport("postinits")
	modimport("stategraphstates")
end

TELEMIGRATE = AddAction("TELEMIGRATE",  GLOBAL.STRINGS.ACTIONS.TELEPORT.TOWNPORTAL, function(act)
    if act.doer ~= nil and act.doer.sg ~= nil then
        local teleporter
        if act.invobject ~= nil then
            if act.doer.sg.currentstate.name == "dolongaction" then
                teleporter = act.invobject
            end
        elseif act.target ~= nil
            and act.doer.sg.currentstate.name == "give" then
            teleporter = act.target
        end
        if teleporter ~= nil and teleporter:HasTag("townportalmigrator") then
            act.doer.sg:GoToState("entershardtownportal", { teleporter = teleporter })
            return true
        end
    end

end)
TELEMIGRATE.rmb = true
TELEMIGRATE.distance = 2

AddComponentAction("SCENE", "lazyworldmigrator", function(inst, doer, actions, right)
	if right and inst:HasTag("townportalmigrator") then
		table.insert(actions, GLOBAL.ACTIONS.TELEMIGRATE)
	end
end)

AddComponentAction("INVENTORY", "lazyworldmigrator", function(inst, doer, actions)
	if inst:HasTag("townportalmigrator") then
		table.insert(actions, GLOBAL.ACTIONS.TELEMIGRATE)
	end
end)

AddStategraphActionHandler("wilson", GLOBAL.ActionHandler(TELEMIGRATE, function(inst, action)
    return action.invobject ~= nil and "dolongaction" or "give"
end))

AddStategraphActionHandler("wilson_client", GLOBAL.ActionHandler(TELEMIGRATE, function(inst, action)
    return action.invobject ~= nil and "dolongaction" or "give"
end))