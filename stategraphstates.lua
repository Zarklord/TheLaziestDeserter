--[[
Copyright (C) 2018 Zarklord

This file is part of The Laziest Deserter.

The Laziest Deserteris free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Laziest Deserter.  If not, see <http://www.gnu.org/licenses/>.
]]

--lets get that nice global enviroment for stategraph states functions
GLOBAL.setfenv(1, GLOBAL)

local function ToggleOffPhysics(inst)
    inst.sg.statemem.isphysicstoggle = true
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.GROUND)
end

local function ToggleOnPhysics(inst)
    inst.sg.statemem.isphysicstoggle = nil
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:CollidesWith(COLLISION.GIANTS)
end

local entershardtownportal = State({
    name = "entershardtownportal",
    tags = { "doing", "busy", "nopredict", "nomorph", "nodangle" },

    onenter = function(inst, data)
        ToggleOffPhysics(inst)
        inst.Physics:Stop()
        inst.components.locomotor:Stop()

        inst.sg.statemem.target = data.teleporter

        inst.AnimState:PlayAnimation("townportal_enter_pre")

        inst.sg.statemem.fx = SpawnPrefab("townportalsandcoffin_fx")
        inst.sg.statemem.fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    end,

    timeline =
    {
        TimeEvent(8 * FRAMES, function(inst)
            inst.sg.statemem.isteleporting = true
            inst.components.health:SetInvincible(true)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end
            inst.DynamicShadow:Enable(false)
        end),
        TimeEvent(18 * FRAMES, function(inst)
            inst:Hide()
        end),
        TimeEvent(26 * FRAMES, function(inst)
            if inst.sg.statemem.target ~= nil and
                inst.sg.statemem.target.components.lazyworldmigrator ~= nil and
                inst.sg.statemem.target.components.lazyworldmigrator:Activate(inst) then
                inst:Hide()
                inst.sg.statemem.fx:KillFX()
            else
                inst.sg:GoToState("exitshardtownportal")
            end
        end),
    },

    onexit = function(inst)
        inst.sg.statemem.fx:KillFX()

        if inst.sg.statemem.isphysicstoggle then
            ToggleOnPhysics(inst)
        end

        if inst.sg.statemem.isteleporting then
            inst.components.health:SetInvincible(false)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst:Show()
            inst.DynamicShadow:Enable(true)
        end
    end,
})

local exitshardtownportal_pre = State({
    name = "exitshardtownportal_pre",
    tags = { "doing", "busy", "nopredict", "nomorph", "nodangle" },

    onenter = function(inst)
        ToggleOffPhysics(inst)
        inst.components.locomotor:Stop()

        inst.sg.statemem.fx = SpawnPrefab("townportalsandcoffin_fx")
        inst.sg.statemem.fx.Transform:SetPosition(inst.Transform:GetWorldPosition())

        inst:Hide()
        inst.components.health:SetInvincible(true)
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:Enable(false)
        end
        inst.DynamicShadow:Enable(false)

        inst.sg:SetTimeout(32 * FRAMES)
    end,

    ontimeout = function(inst)
        inst.sg:GoToState("exitshardtownportal")
    end,

    onexit = function(inst)
        inst.sg.statemem.fx:KillFX()

        if inst.sg.statemem.isphysicstoggle then
            ToggleOnPhysics(inst)
        end

        inst:Show()
        inst.components.health:SetInvincible(false)
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:Enable(true)
        end
        inst.DynamicShadow:Enable(true)
    end,
})

local exitshardtownportal = State({
    name = "exitshardtownportal",
    tags = { "doing", "busy", "nopredict", "nomorph", "nodangle" },

    onenter = function(inst)
        ToggleOffPhysics(inst)
        inst.components.locomotor:Stop()

        inst.AnimState:PlayAnimation("townportal_exit_pst")
    end,

    timeline =
    {
        TimeEvent(18 * FRAMES, function(inst)
            if inst.sg.statemem.isphysicstoggle then
                ToggleOnPhysics(inst)
            end
        end),
        TimeEvent(26 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("nopredict")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },

    onexit = function(inst)
        if inst.sg.statemem.isphysicstoggle then
            ToggleOnPhysics(inst)
        end
    end,
})

MODENV.AddStategraphState("wilson", entershardtownportal)
MODENV.AddStategraphState("wilson", exitshardtownportal_pre)
MODENV.AddStategraphState("wilson", exitshardtownportal)